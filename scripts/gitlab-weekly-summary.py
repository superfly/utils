#!/usr/bin/env python3
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4 colorcolumn=120
import json
import sys
from argparse import ArgumentParser
from datetime import datetime, timedelta
from gitlab import Gitlab
from gitlab.exceptions import GitlabListError

TEXT_SUMMARY = """Weekly Report: {week}
-------------------------

* {commits} commits by {authors} authors
* {new} new issues this week
* {closed} issues closed this week
* {open} issues still open
"""
ALL_CLOSED = """* {all_closed} total issues closed
"""
LABEL_SECTION = """
Labels:
{labels}
"""
LABEL_LINE = '* {label} ({count})'


def get_projects(gitlab_url, gitlab_auth=None, group=None, project_list=None):
    """
    Get all the projects in GitLab
    """
    gitlab_auth = gitlab_auth or {}
    project_list = project_list or []
    projects = []
    gl = Gitlab(gitlab_url, **gitlab_auth)
    if gitlab_auth:
        gl.auth()
    if project_list:
        for project_url in project_list:
            projects.append(gl.projects.get(project_url.replace('/', '%2F')))
    elif group:
        group_projects = gl.groups.get(group.replace('/', '%2F')).projects.list(as_list=False)
        projects = [gl.projects.get(gp.id) for gp in group_projects]
    else:
        projects = [project for project in gl.projects.list(as_list=False)]
    return projects


def get_commits(project, since=None):
    """
    Get the list of commits, optionally since a particular date or time
    """
    if since and isinstance(since, datetime):
        since = since.isoformat()
    try:
        return project.commits.list(all=True, since=since)
    except GitlabListError:
        return []


def get_issues(project, since=None, all_closed=False):
    """
    Get the issues
    """
    if since and isinstance(since, datetime):
        since = since.isoformat()
    new_issues = project.issues.list(all=True, state='opened', created_after=since)
    open_issues = project.issues.list(all=True, state='opened', created_before=since)
    closed_issues = project.issues.list(all=True, state='closed', updated_after=since)
    if all_closed:
        all_closed_issues = [issue for issue in project.issues.list(as_list=False, state='closed')]
    else:
        all_closed_issues = []
    return new_issues, open_issues, closed_issues, all_closed_issues


def get_label_summary(issues):
    """
    Build a summary of the labels
    """
    label_summary = {}
    for issue in issues:
        for label in issue.labels:
            try:
                label_summary[label] += 1
            except KeyError:
                label_summary[label] = 1
    return label_summary


def merge_summaries(old_summary, new_summary):
    """
    Merge the amounts in 2 summaries
    """
    for key, value in new_summary.items():
        try:
            old_summary[key] += new_summary[key]
        except KeyError:
            old_summary[key] = new_summary[key]


def get_author_count(commits):
    """
    Return the unique number of authors
    """
    authors = set()
    for commit in commits:
        authors.add(commit.committer_email)
    return len(authors)


def build_summary(projects, since=None, all_closed=False, show_labels=True):
    """
    Build a summary of all commits and issues
    """
    summary = {
        'since': since.strftime('%Y-%m-%d') if since else None,
        'commits': {
            'count': 0,
            'authors': 0
        },
        'issues': {
            'new': 0,
            'open': 0,
            'closed': 0,
        }
    }
    if all_closed:
        summary['issues']['all_closed'] = 0
    if show_labels:
        summary['issues']['labels'] = {}
    for project in projects:
        commits = get_commits(project, since)
        new_issues, open_issues, closed_issues, all_closed_issues = get_issues(project, since, all_closed)
        summary['commits']['count'] += len(commits)
        summary['commits']['authors'] += get_author_count(commits)
        summary['issues']['new'] += len(new_issues)
        summary['issues']['open'] += len(open_issues)
        summary['issues']['closed'] += len(closed_issues)
        if all_closed:
            summary['issues']['all_closed'] += len(all_closed_issues)
        if show_labels:
            merge_summaries(summary['issues']['labels'], get_label_summary(new_issues))
            merge_summaries(summary['issues']['labels'], get_label_summary(open_issues))
    return summary


def build_text_output(summary, since):
    """
    Build the text format output
    """
    text_summary = TEXT_SUMMARY.format(week=since.strftime('%Y-%m-%d'), commits=summary['commits']['count'],
                                       authors=summary['commits']['authors'], new=summary['issues']['new'],
                                       closed=summary['issues']['closed'], open=summary['issues']['open'])
    if summary['issues'].get('all_closed'):
        text_summary += ALL_CLOSED.format(all_closed=summary['issues']['all_closed'])
    if summary['issues'].get('labels'):
        label_lines = [LABEL_LINE.format(count=value, label=key)
                       for key, value in summary['issues']['labels'].items()]
        text_summary += LABEL_SECTION.format(labels='\n'.join(label_lines))
    return text_summary


def get_args():
    """
    Build a command line argument parser, and parse the arguments
    """
    parser = ArgumentParser()
    parser.add_argument('-u', '--url', metavar='BASE_URL', default='https://gitlab.com',
                        help='Base URL of GitLab instance. Defaults to https://gitlab.com')
    parser.add_argument('-a', '--auth', metavar='TYPE=TOKEN',
                        help='Authentication details, in the TYPE=TOKEN format')
    parser.add_argument('-g', '--group', metavar='GROUP',
                        help='Select projects from a group. Ignored when --project or '
                             '--project-file are specified')
    parser.add_argument('-p', '--project', metavar='PROJECT', action='append',
                        help='Project to summarise. Specify multiple times for multiple projects')
    parser.add_argument('-l', '--project-file', metavar='FILENAME',
                        help='A file containing a list of projects')
    parser.add_argument('-s', '--since', metavar='DATETIME',
                        help='Specify a datetime in ISO format, defaults to the most recent Sunday'
                             ' at midnight')
    parser.add_argument('-f', '--format', metavar='FORMAT', help='Output format, either "text" or '
                        '"json", defaults to "text"')
    parser.add_argument('--all-closed', action='store_true', help='Also display the total number of closed issues')
    parser.add_argument('--no-labels', action='store_true', help='Don\'t count or output labels')
    return parser.parse_args()


def main():
    """
    Run the thing
    """
    args = get_args()
    # Build project list
    if args.project_file:
        project_list = open(args.project_file).readlines()
    else:
        project_list = args.project
    # Set up the since
    if args.since:
        since = datetime.fromisoformat(args.since)
    else:
        now = datetime.now()
        since = (now - timedelta(days=now.isoweekday() % 7)).replace(hour=0, minute=0, second=0,
                                                                     microsecond=0)
    # Set up auth
    if args.auth:
        key, value = args.auth.split('=', maxsplit=1)
        auth = {key: value}
    else:
        auth = {}
    # Now go get all the information
    projects = get_projects(args.url, auth, args.group, project_list)
    if not projects:
        print('No projects available')
        return 1
    summary = build_summary(projects, since, all_closed=args.all_closed, show_labels=not args.no_labels)
    if args.format and args.format.lower() == 'json':
        print(json.dumps(summary))
    else:
        print(build_text_output(summary, since))
    return 0


if __name__ == '__main__':
    sys.exit(main())
