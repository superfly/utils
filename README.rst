Utilities
=========

Various scripts, configs and other utilities I have written.

License
-------

Unless otherwise stated, all of these are MIT licensed.


Scripts
-------

**gitlab-weekly-summary.py**

.. code::

   $ python3 scripts/gitlab-weekly-summary.py -h                         
   usage: gitlab-weekly-summary.py [-h] [-u BASE_URL] [-a TYPE=TOKEN] [-g GROUP] [-p PROJECT] [-l FILENAME] [-s DATETIME] [-f FORMAT] [--all-closed] [--no-labels]

   optional arguments:
     -h, --help            show this help message and exit
     -u BASE_URL, --url BASE_URL
                           Base URL of GitLab instance. Defaults to https://gitlab.com
     -a TYPE=TOKEN, --auth TYPE=TOKEN
                           Authentication details, in the TYPE=TOKEN format
     -g GROUP, --group GROUP
                           Select projects from a group. Ignored when --project or --project-file are specified
     -p PROJECT, --project PROJECT
                           Project to summarise. Specify multiple times for multiple projects
     -l FILENAME, --project-file FILENAME
                           A file containing a list of projects
     -s DATETIME, --since DATETIME
                           Specify a datetime in ISO format, defaults to the most recent Sunday at midnight
     -f FORMAT, --format FORMAT
                           Output format, either "text" or "json", defaults to "text"
     --all-closed          Also display the total number of closed issues
     --no-labels           Don't count or output labels
