Changelog
=========

2020-06-05
----------

* Update README with information about weekly summary script
* Update GitLab weekly summary script

  * Add argume to include total closed issues
  * Reformat label lines
  * Add argument to disable label summary

2020-06-03
----------

* Created repository
* Added LICENSE file
* Added and updated README
* Added scripts/gitlab-weekly-summary.py
* Added config/init.vim
